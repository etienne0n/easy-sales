package com.etienne0n.easysales.webcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.etienne0n.easysales.domain.dto.ProduktBearbeitenWebDTO;
import com.etienne0n.easysales.domain.dto.ProduktEintragenWebDTO;
import com.etienne0n.easysales.domain.entities.Produkt;
import com.etienne0n.easysales.exceptions.ProduktExistiertNichtException;
import com.etienne0n.easysales.service.ProduktService;

@Controller
@RequestMapping("produkte/")
public class ProduktController {
	
	private ProduktService produktService;
	
	@Autowired
	public ProduktController(ProduktService produktService) {
		this.produktService = produktService;
	}
	

	@GetMapping("produkt_eintragen")
	public String produktEintragen() {
		return "produkt_eintragen";
	}
	
	@PostMapping("produkt_eintragen_proc")
	public String produktEintragenProc(Model model, ProduktEintragenWebDTO dto){
		String msg;
		if(produktService.produktEintragen(dto)) {
			msg = "Produkt angelegt";
		}else {
			msg = "ungueltige Eingabe!";
		}
		
		model.addAttribute("message", "[" + msg + "]");
		return produktEintragen();
	}
	
	
	@GetMapping("alle_produkte_anzeigen")
	public String alleKundenAnzeigen(Model model) {
		List<Produkt> produkte = produktService.getAlleProdukte();
		
		model.addAttribute("alle_produkte", produkte);
		return "produkte_anzeigen";
	}
	
	@PostMapping("produkt_bearbeiten")
	public String produktBearbeiten(Model model, ProduktBearbeitenWebDTO dto) {
		model.addAttribute("produkt", dto);
		return "produkt_bearbeiten";
	}
	
	@PostMapping("produkt_bearbeiten_proc")
	public String produktBearbeitenProc(Model model, ProduktBearbeitenWebDTO dto) 
			throws ProduktExistiertNichtException {
		Produkt p = null;
		String msg = "";
		p = produktService.getProdukt(dto.getId());
		
		if(p != null) {
			p.bearbeiten(dto);
			if(produktService.produktEintragen(p)) {
				msg = "Produkt erfolgreich geändert.";
			}else {
				msg = "Validierung fehlgeschlagen! Produkt konnte nicht geändert werden!";
			}
		}
		model.addAttribute("message", msg);
		return "/message";
	}
	
	
}
