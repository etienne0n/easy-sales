package com.etienne0n.easysales.webcontroller;

import static com.etienne0n.easysales.util.DatenKonstanten.TEL_MIN_LEN;
import static com.etienne0n.easysales.util.DatenKonstanten.TEL_REGEX;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.etienne0n.easysales.domain.dto.EmailBearbeitenDTO;
import com.etienne0n.easysales.domain.dto.KundeBearbeitenWebDTO;
import com.etienne0n.easysales.domain.dto.KundeEintragenWebDTO;
import com.etienne0n.easysales.domain.entities.Kunde;
import com.etienne0n.easysales.exceptions.KundeExistiertNichtException;
import com.etienne0n.easysales.service.KundeService;

@Controller
@RequestMapping("kunden/")
public class KundeController {

	
	private KundeService kundeService;
	
	// KONSTRUKTOR
	@Autowired
	public KundeController(KundeService kundeService) {
		this.kundeService = kundeService;
	}
	
	// KUNDE EINTRAGEN
	@GetMapping("kunde_eintragen")
	public String kundeEintragen(Model model) {
		model.addAttribute("now", new Date(System.currentTimeMillis()));
		model.addAttribute("tel_pattern", TEL_REGEX);
		model.addAttribute("tel_min_len", TEL_MIN_LEN);
		
		return "kunde_eintragen";
	}
	// KUNDE EINTRAGEN PROZEDUR
	@PostMapping("kunde_eintragen_proc")
	public String kundeEintragenProzedur(Model model, KundeEintragenWebDTO kundeWebDTO) {
		String msg;
		if(!kundeService.kundeEintragen(kundeWebDTO)) {
			msg = "ungueltige Eingabe!";	
		}else {
			msg = "Kunde angelegt";
		}
		model.addAttribute("message", "[" + msg + "]");
		return kundeEintragen(model);
	}
	
	// GET ALLE KUNDEN
	@GetMapping("alle_kunden_anzeigen")
	public String alleKundenAnzeigen(Model model) {
		List<Kunde> kunden = kundeService.getAlleKunden();

		
		model.addAttribute("alle_kunden", kunden);
		return "kunden_anzeigen";
	}
	
	// KUNDE BEARBEITEN
	
	@PostMapping("kunde_bearbeiten")
	public String kundeBearbeiten(Model model, KundeBearbeitenWebDTO dto,
			Long[] email_ids, Long[] telefon_ids){

	
		
		model.addAttribute("kunde", dto);
		return "kunde_bearbeiten";
	}
	
	@PostMapping("kunde_bearbeiten_proc")
	public String kundeBearbeitenProc(KundeBearbeitenWebDTO dto, 
			Model model) throws KundeExistiertNichtException {
		Kunde k = null;
		String msg = "";
		k = kundeService.getKunde(dto.getId());
	
		if(k != null) {
			k.bearbeiten(dto);
			if(kundeService.kundeEintragen(k)) {
				msg = "Kunde erfolgreich geändert.";
			}else {
				msg = "Validierung fehlgeschlagen! Kunde konnte nicht geändert werden!";
			}
		}
		model.addAttribute("message", "[" + msg + "]");
		return "/message";
	}
	
}
