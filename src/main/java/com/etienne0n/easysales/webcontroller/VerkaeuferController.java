package com.etienne0n.easysales.webcontroller;

import static com.etienne0n.easysales.util.DatenKonstanten.TEL_MIN_LEN;
import static com.etienne0n.easysales.util.DatenKonstanten.TEL_REGEX;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.etienne0n.easysales.domain.dto.VerkaeuferBearbeitenWebDTO;
import com.etienne0n.easysales.domain.dto.VerkaeuferEintragenWebDTO;
import com.etienne0n.easysales.domain.entities.Verkaeufer;
import com.etienne0n.easysales.exceptions.VerkaeuferExistiertNichtException;
import com.etienne0n.easysales.service.VerkaeuferService;

@Controller
@RequestMapping("verkaeufer/")
public class VerkaeuferController {
	
	private VerkaeuferService verkaeuferService;
	
	// CONSTRUCTOR
	@Autowired
	public VerkaeuferController(VerkaeuferService verkaeuferService) {
		this.verkaeuferService = verkaeuferService;
	}
	
	
	// ALLE VERKAEUFER
	@GetMapping("alle_verkaeufer_anzeigen")
	public String alleVerkaeuferAnzeigen(Model model) {
		
		List<Verkaeufer> verkaeufer = verkaeuferService.getAlleVerkaeufer();

		model.addAttribute("alle_verkaeufer", verkaeufer);
		return "verkaeufer_anzeigen";
	}
	
	// VERKAEUFER ANLEGEN MASKE
	@GetMapping("verkaeufer_eintragen")
	public String verkaeuferEintragen(Model model) {
		model.addAttribute("now", new Date(System.currentTimeMillis()));
		model.addAttribute("tel_pattern", TEL_REGEX);
		model.addAttribute("tel_min_len", TEL_MIN_LEN);
		return "verkaeufer_eintragen";
	}
	
	// VERKAEUFER ANLEGEN PROZEDUR
	@PostMapping("verkaeufer_eintragen_proc")
	public String verkaeuferEintragenProzedur(Model model, VerkaeuferEintragenWebDTO verkaeuferWebDTO){
		String msg;
		if(verkaeuferService.verkaeuferEintragen(verkaeuferWebDTO)) {
			msg = "Verkäufer angelegt";
		}else {
			msg = "Ungültige Eingabe!";
		}
		model.addAttribute("message", "[" + msg + "]");
		
		return verkaeuferEintragen(model);
	}
	
	@PostMapping("verkaeufer_bearbeiten")
	public String verkaeuferBearbeiten(Model model, VerkaeuferBearbeitenWebDTO verkaeuferAendernWebDTO){
		model.addAttribute("verkaeufer", verkaeuferAendernWebDTO);
		return "verkaeufer_bearbeiten";
	}
	
	@PostMapping("verkaeufer_bearbeiten_proc")
	public String  kundeBearbeitenProc(VerkaeuferBearbeitenWebDTO verkaeufernAendernWebDTO, 
			Model model) throws VerkaeuferExistiertNichtException {
		Verkaeufer v = null;
		String msg = "";
		v = verkaeuferService.getVerkaeufer(verkaeufernAendernWebDTO.getId());
	
		if(v != null) {
			v.bearbeiten(verkaeufernAendernWebDTO);
			if(verkaeuferService.verkaeuferEintragen(v)) {
				msg = "Verkäufer erfolgreich geändert.";
			}else {
				msg = "Validierung fehlgeschlagen! Verkäufer konnte nicht geändert werden!";
			}
		}
		model.addAttribute("message", "[" + msg + "]");
		return "/message";
	}
	
	
}
