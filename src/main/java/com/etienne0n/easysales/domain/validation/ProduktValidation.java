package com.etienne0n.easysales.domain.validation;

import org.springframework.stereotype.Component;

import com.etienne0n.easysales.domain.entities.Produkt;

@Component
public class ProduktValidation extends AbstractValidation{
	
	public boolean validate(Produkt produkt) {
		return produkt != null && nameValidation(produkt.getName());	
	}
}
