package com.etienne0n.easysales.domain.validation;

abstract class AbstractValidation {
	
	boolean nameValidation(String name) {
		return name != null && !name.equals("");
	}
}
