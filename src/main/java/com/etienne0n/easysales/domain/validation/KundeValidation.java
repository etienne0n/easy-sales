package com.etienne0n.easysales.domain.validation;

import org.springframework.stereotype.Component;

import com.etienne0n.easysales.domain.entities.Kunde;
@Component
public class KundeValidation extends AbstractValidation{
	
	public boolean validate(Kunde kunde) {
	
		return kunde != null && nameValidation(kunde.getVorname())
				&& nameValidation(kunde.getNachname())
				&& nameValidation(kunde.getFirma())
				&& nameValidation(kunde.getPlz())
				&& strasseHausnummerValidation(kunde.getStrasseHausnummer())
				;
	}
	
	private boolean strasseHausnummerValidation(String strasseHausnummer) {
		return nameValidation(strasseHausnummer);
	}

	
}
