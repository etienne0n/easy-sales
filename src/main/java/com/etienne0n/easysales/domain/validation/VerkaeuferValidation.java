package com.etienne0n.easysales.domain.validation;

import org.springframework.stereotype.Component;

import com.etienne0n.easysales.domain.entities.Verkaeufer;

@Component
public class VerkaeuferValidation extends AbstractValidation{
	
	
	public boolean validate(Verkaeufer verkaeufer) {
		return verkaeufer != null && nameValidation(verkaeufer.getVorname())
				&& nameValidation(verkaeufer.getNachname())
				&& nameValidation(verkaeufer.getPlz())
				&& strasseHausnummerValidation(verkaeufer.getStrasseHausnummer())
				;
	}
	
	private boolean strasseHausnummerValidation(String strasseHausnummer) {
		return nameValidation(strasseHausnummer);
	}

	
}
