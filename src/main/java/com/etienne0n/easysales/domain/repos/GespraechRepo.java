package com.etienne0n.easysales.domain.repos;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.etienne0n.easysales.domain.entities.Gespraech;
@Repository
public interface GespraechRepo extends JpaRepository<Gespraech, Long> {

	public List<Gespraech> findByDatumBefore(Date date);

	public List<Gespraech> findByDatumAfter(Date date);
//	@formatter:off	
	@Query("SELECT DISTINCT g FROM Kunde k, Gespraech g, Verkaeufer v, Thema t "
			+ "WHERE "
			+ "g.kunde.id = k.id AND "
			+ "g.verkaeufer.id = v.id AND "
			+ "g.id = t.gespraech.id AND "
			+ "t.wahrscheinlichkeit < 100 AND t.wahrscheinlichkeit > 0 "
			+ "ORDER BY g.verkaeufer.id"
			)
	public List<Gespraech> findeAlleUnabgeschlossenen();

	@Query("SELECT DISTINCT g FROM Kunde k, Gespraech g, Verkaeufer v, Thema t "
			+ "WHERE "
			+ "g.datum < ?1 AND "
			+ "g.kunde.id = k.id AND "
			+ "g.verkaeufer.id = v.id AND "
			+ "g.id = t.gespraech.id AND "
			+ "t.wahrscheinlichkeit < 100 AND t.wahrscheinlichkeit > 0 "
			+ "ORDER BY g.verkaeufer.id"
			)
	public List<Gespraech> findeAlleUnabgeschlossenenVor(Date date);

	@Query("SELECT g FROM Kunde k, Gespraech g, Verkaeufer v, Thema t "
			+ "WHERE "
			+ "g.kunde.id = k.id AND "
			+ "g.verkaeufer.id = v.id AND "
			+ "g.id = t.gespraech.id AND "
			+ "t.wahrscheinlichkeit = ?1" 
			)
	public List<Gespraech> findeAlleMitWahrscheinlichkeitX(int x);


	@Query("SELECT g FROM Kunde k, Gespraech g, Verkaeufer v, Thema t "
			+ "WHERE "
			+ "g.kunde.id = k.id AND "
			+ "g.verkaeufer.id = v.id AND "
			+ "g.id = t.gespraech.id AND "
			+ "t.wahrscheinlichkeit >= ?1 AND t.wahrscheinlichkeit < ?2" 
			)
	public List<Gespraech> findeAlleMitWahrscheinlichkeitGroesserGleichXUndKleinerY(int x, int y);
//	@formatter:on
}
