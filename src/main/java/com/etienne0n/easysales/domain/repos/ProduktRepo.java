package com.etienne0n.easysales.domain.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.etienne0n.easysales.domain.entities.Produkt;
@Repository
public interface ProduktRepo extends JpaRepository<Produkt, Long> {

}
