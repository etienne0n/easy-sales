package com.etienne0n.easysales.domain.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.etienne0n.easysales.domain.entities.Verkaeufer;

@Repository
public interface VerkaeuferRepo extends JpaRepository<Verkaeufer, Long> {

}
