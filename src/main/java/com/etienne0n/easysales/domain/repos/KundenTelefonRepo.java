package com.etienne0n.easysales.domain.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.etienne0n.easysales.domain.entities.KundeTelefon;
@Repository
public interface KundenTelefonRepo extends JpaRepository<KundeTelefon, Long> {
	public Optional<KundeTelefon> findByKundeId(Long id);
}
