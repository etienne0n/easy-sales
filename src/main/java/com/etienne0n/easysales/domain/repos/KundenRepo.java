package com.etienne0n.easysales.domain.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.etienne0n.easysales.domain.entities.Kunde;
@Repository
public interface KundenRepo extends JpaRepository<Kunde, Long>{
	
	public List<Kunde> findByVornameIgnoreCase(String vorname);
	public List<Kunde> findByNachnameIgnoreCase(String nachname);
	public List<Kunde> findByNachnameAndVornameIgnoreCase(String nachname, String vorname);
	public List<Kunde> findByFirmaIgnoreCase(String firma);
	public List<Kunde> findByPlz(String plz);
	
		
}
