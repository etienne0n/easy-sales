package com.etienne0n.easysales.domain.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.etienne0n.easysales.domain.entities.KundeEmail;
@Repository
public interface KundenEmailRepo extends JpaRepository<KundeEmail, Long> {
	public Optional<KundeEmail> findByKundeId(Long id);
}
