package com.etienne0n.easysales.domain.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etienne0n.easysales.domain.entities.VerkaeuferEmail;

public interface VerkaeuferEmailRepo extends JpaRepository<VerkaeuferEmail, Long> {
	public Optional<VerkaeuferEmail> findByVerkaeuferId(Long id);
}
