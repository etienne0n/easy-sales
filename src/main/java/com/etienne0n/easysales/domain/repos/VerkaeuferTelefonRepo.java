package com.etienne0n.easysales.domain.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etienne0n.easysales.domain.entities.VerkaeuferTelefon;

public interface VerkaeuferTelefonRepo extends JpaRepository<VerkaeuferTelefon, Long> {
	public Optional<VerkaeuferTelefon> findByVerkaeuferId(Long id);
}
