package com.etienne0n.easysales.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class VerkaeuferEintragenWebDTO extends VerkaeuferBearbeitenWebDTO{
	private String beschaeftigtSeit;
	private String telefonnummer1;
	private String telefonnummer2;
	private String telefonnummer3;
	private String email1;
	private String email2;
	private String email3;
}
