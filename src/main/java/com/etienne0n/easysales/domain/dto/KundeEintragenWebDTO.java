package com.etienne0n.easysales.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class KundeEintragenWebDTO{
	private String vorname;
	private String nachname;
	private String plz;
	private String firma;
	private String strasseHausnummer;
	private String kundeSeit;
	private String telefonnummer1;
	private String telefonnummer2;
	private String telefonnummer3;
	private String email1;
	private String email2;
	private String email3;

}
