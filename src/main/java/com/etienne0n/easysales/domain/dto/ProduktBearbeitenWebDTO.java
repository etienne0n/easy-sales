package com.etienne0n.easysales.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProduktBearbeitenWebDTO {
	private Long id;
	private String name;
}
