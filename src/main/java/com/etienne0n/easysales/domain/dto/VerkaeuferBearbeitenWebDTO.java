package com.etienne0n.easysales.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class VerkaeuferBearbeitenWebDTO {
	private Long id;
	private String vorname;
	private String nachname;
	private String plz;
	private String strasseHausnummer;
}
