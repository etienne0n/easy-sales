package com.etienne0n.easysales.domain.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.etienne0n.easysales.domain.entities.abstractentities.Email;

@Entity
public class VerkaeuferEmail extends Email{
	@ManyToOne(fetch=FetchType.EAGER)
	private Verkaeufer verkaeufer;
}
