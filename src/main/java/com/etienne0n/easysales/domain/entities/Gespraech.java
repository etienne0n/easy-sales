package com.etienne0n.easysales.domain.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
public class Gespraech {
	
	@Id
	private Long id;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)	
	private Date datum;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Verkaeufer verkaeufer;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Kunde kunde;

	@OneToMany(fetch=FetchType.EAGER, mappedBy="gespraech")
	private Set<Thema> themen = new HashSet<>();
	
	
	
	
}
