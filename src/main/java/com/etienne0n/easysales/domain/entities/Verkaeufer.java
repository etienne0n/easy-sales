package com.etienne0n.easysales.domain.entities;

import static com.etienne0n.easysales.util.Dates.DEFAULT_DATE_PATTERN;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.etienne0n.easysales.domain.dto.VerkaeuferBearbeitenWebDTO;
import com.etienne0n.easysales.domain.dto.VerkaeuferEintragenWebDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Verkaeufer {
	
	@Id
	@GeneratedValue
	private Long id;
	@Column(length=45, nullable=false)
	private String vorname;
	@Column(length=45, nullable=false)
	private String nachname;
	
	@Column(length=45, nullable=false)
	private String plz;
	@Column(length=100, nullable=false)
	private String StrasseHausnummer;
	
	@Column(name="beschaeftigt_seit")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date beschaeftigtSeit;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="verkaeufer_id")
	private Set<VerkaeuferEmail> emailAdressen = new HashSet<>();
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="verkaeufer_id")
	private Set<VerkaeuferTelefon> telefonNummern = new HashSet<>();
	
	public Verkaeufer() {super();}
	
	
	public Verkaeufer(VerkaeuferEintragenWebDTO dto) {
		super();
		this.vorname = dto.getVorname();
		this.nachname = dto.getNachname();
		this.plz = dto.getPlz();
		this.StrasseHausnummer = dto.getStrasseHausnummer();
		try {
			DateFormat df = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
			this.beschaeftigtSeit = df.parse(dto.getBeschaeftigtSeit());
		} catch (ParseException e) {
			// Wenn Datum nicht geparsed werden kann. Beschaeftigt seit jetzt!
			this.beschaeftigtSeit = new Date(System.currentTimeMillis());
			e.printStackTrace();
		}
		addEmail(dto.getEmail1());
		addEmail(dto.getEmail2());
		addEmail(dto.getEmail3());
		addTelefon(dto.getTelefonnummer1());
		addTelefon(dto.getTelefonnummer2());
		addTelefon(dto.getTelefonnummer3());
	}
	
	public void bearbeiten(VerkaeuferBearbeitenWebDTO dto) {
		this.vorname = dto.getVorname();
		this.nachname = dto.getNachname();
		this.plz = dto.getPlz();
		this.StrasseHausnummer = dto.getStrasseHausnummer();

	}
	
	public void addEmail(VerkaeuferEmail email) {
		emailAdressen.add(email);
	}
	
	public void addEmail(String email) {
		if(!email.equals("") && email != null) {
			VerkaeuferEmail emailInstance = new VerkaeuferEmail();
			emailInstance.setEmail(email);
			addEmail(emailInstance);
		}else {
			return;
		}
	}

	public void addTelefon(VerkaeuferTelefon telefon) {
		telefonNummern.add(telefon);	
	}
	
	public void addTelefon(String telefonnummer) {
		if(!telefonnummer.equals("") && telefonnummer != null) {
			VerkaeuferTelefon telefonInstance = new VerkaeuferTelefon();
			telefonInstance.setTelefonnummer(telefonnummer);
			addTelefon(telefonInstance);
		}else {
			return;
		}
	}


	


	
}
