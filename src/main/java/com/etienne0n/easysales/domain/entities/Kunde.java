package com.etienne0n.easysales.domain.entities;

import static com.etienne0n.easysales.util.Dates.DEFAULT_DATE_PATTERN;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.etienne0n.easysales.domain.dto.KundeBearbeitenWebDTO;
import com.etienne0n.easysales.domain.dto.KundeEintragenWebDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
public class Kunde{
	
	@Id
	@GeneratedValue
	private Long id;
	@Column(length=45, nullable=false)
	private String vorname;
	@Column(length=45, nullable=false)
	private String nachname;
	@Column(length=45, nullable=false)
	private String firma;
	@Column(length=45, nullable=false)
	private String plz;
	@Column(length=100, nullable=false)
	private String StrasseHausnummer;

	@Column(name="kunde_seit")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date kundeSeit;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="kunde_id")
	private Set<KundeEmail> emailAdressen = new HashSet<>();
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="kunde_id")
	private Set<KundeTelefon> telefonNummern = new HashSet<>();
	
	public Kunde() {super();}
	
	public Kunde(KundeEintragenWebDTO dto) {
		super();
		this.vorname = dto.getVorname();
		this.nachname = dto.getNachname();
		this.firma = dto.getFirma();
		this.plz = dto.getPlz();
		this.StrasseHausnummer = dto.getStrasseHausnummer();
		DateFormat df = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
		try {
			this.kundeSeit = df.parse(dto.getKundeSeit());
		} catch (ParseException e) {
			// Wenn Datum nicht geparsed werden kann. Kunde seit jetzt!
			this.kundeSeit = new Date(System.currentTimeMillis());
			e.printStackTrace();
		}
		addEmail(dto.getEmail1());
		addEmail(dto.getEmail2());
		addEmail(dto.getEmail3());
		addTelefon(dto.getTelefonnummer1());
		addTelefon(dto.getTelefonnummer2());
		addTelefon(dto.getTelefonnummer3());
	}

	
	public void bearbeiten(KundeBearbeitenWebDTO dto) {
		this.vorname = dto.getVorname();
		this.nachname = dto.getNachname();
		this.firma = dto.getFirma();
		this.plz = dto.getPlz();
		this.StrasseHausnummer = dto.getStrasseHausnummer();
	}
	

	
	public void addEmail(KundeEmail email) {
		emailAdressen.add(email);
	}
	
	public void addEmail(String email) {
		if(email != null && !email.equals("")) {
			KundeEmail emailInstance = new KundeEmail();
			emailInstance.setEmail(email);
			addEmail(emailInstance);
		}
	}

	public void addTelefon(KundeTelefon telefon) {
		telefonNummern.add(telefon);	
	}
	
	public void addTelefon(String telefonnummer) {
		if(telefonnummer != null && !telefonnummer.equals("")) {
			KundeTelefon telefonInstance = new KundeTelefon();
			telefonInstance.setTelefonnummer(telefonnummer);
			addTelefon(telefonInstance);
		}
	}
	
	


	
}
