package com.etienne0n.easysales.domain.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.etienne0n.easysales.domain.entities.abstractentities.Telefon;

@Entity
public class KundeTelefon extends Telefon{
	@ManyToOne(fetch=FetchType.EAGER)
	private Kunde kunde;
}

