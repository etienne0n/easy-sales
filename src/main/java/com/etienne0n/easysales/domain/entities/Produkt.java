package com.etienne0n.easysales.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.etienne0n.easysales.domain.dto.ProduktBearbeitenWebDTO;
import com.etienne0n.easysales.domain.dto.ProduktEintragenWebDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
public class Produkt {
	@Id
	@GeneratedValue
	private Long id;
	@Column(length=45, nullable=false)
	private String name;
	
	public Produkt() {}
	
	public Produkt(ProduktEintragenWebDTO dto) {
		this.name = dto.getName();
	}
	
	
	
	public void bearbeiten(ProduktBearbeitenWebDTO dto) {
		this.name = dto.getName();
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produkt other = (Produkt) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
	
	
	
	
}
