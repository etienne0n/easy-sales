package com.etienne0n.easysales.domain.entities.abstractentities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Telefon {
	@Id
	@GeneratedValue
	private Long id;
	@Column(length=45, nullable=false, unique=true)
	private String telefonnummer;
	
	// @formatter:off
	// SETTER AND GETTER 
	// ==============================================================================
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	public String getTelefonnummer() { return telefonnummer;}
	public void setTelefonnummer(String telefonnummer) {this.telefonnummer = telefonnummer;}
	// @formatter:on
	// ==============================================================================
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((telefonnummer == null) ? 0 : telefonnummer.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefon other = (Telefon) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (telefonnummer == null) {
			if (other.telefonnummer != null)
				return false;
		} else if (!telefonnummer.equals(other.telefonnummer))
			return false;
		return true;
	}
	
	// ==============================================================================
	// TO_STRING
	// ==============================================================================
	@Override
	public String toString() {
		return "Telefon [id=" + id + ", telefonnummer=" + telefonnummer + "]";
	}
	
}
