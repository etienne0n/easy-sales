package com.etienne0n.easysales.domain.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.etienne0n.easysales.domain.entities.abstractentities.Telefon;

@Entity
public class VerkaeuferTelefon extends Telefon{
	@ManyToOne(fetch=FetchType.EAGER)
	private Verkaeufer verkaeufer;
}
