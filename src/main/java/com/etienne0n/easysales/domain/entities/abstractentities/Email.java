package com.etienne0n.easysales.domain.entities.abstractentities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public abstract class Email {
	@Id
	@GeneratedValue
	private Long id;
	@Column(length=45, unique=true, nullable=false)
	private String email;
	
	// @formatter:off
	// SETTER AND GETTER 
	// ==============================================================================
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	public String getEmail() { return email;}
	public void setEmail(String email) {this.email = email;}
	// @formatter:on
	// ==============================================================================
	
	// HASHCODE AND EQUALS
	// ==============================================================================
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Email other = (Email) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	// ==============================================================================
	// TO_STRING
	// ==============================================================================
	@Override
	public String toString() {
		return "Email [id=" + id + ", email=" + email + "]";
	}
	
}
