package com.etienne0n.easysales.domain.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Getter;
import lombok.Setter;
import static com.etienne0n.easysales.util.DatenKonstanten.*;

@Getter
@Setter
@Entity
public class Thema {
	@Id
	@GeneratedValue
	private Long id;
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Produkt> produkte = new HashSet<>();
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Gespraech gespraech;
	
	@Column(length=BESCHREIBUNG_MAX_LEN, nullable=false)
	private String beschreibung;
	
	/**
	 * Angaben in cent
	 */
	@Min(0)
	private long auftragswert;
	
	@Min(0)
	@Max(100)
	private int wahrscheinlichkeit;

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gespraech == null) ? 0 : gespraech.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Thema other = (Thema) obj;
		if (gespraech == null) {
			if (other.gespraech != null)
				return false;
		} else if (!gespraech.equals(other.gespraech))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

	@Override
	public String toString() {
		return "Thema [id=" + id + ", produkte=" + produkte + ", gespraech_id=" + gespraech.getId() + ", beschreibung="
				+ beschreibung + ", auftragswert=" + auftragswert + ", wahrscheinlichkeit=" + wahrscheinlichkeit + "]";
	}





	
	
	
	
	
	
}
