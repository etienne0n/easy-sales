package com.etienne0n.easysales.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etienne0n.easysales.domain.dto.KundeEintragenWebDTO;
import com.etienne0n.easysales.domain.entities.Kunde;
import com.etienne0n.easysales.domain.repos.KundenRepo;
import com.etienne0n.easysales.domain.validation.KundeValidation;
import com.etienne0n.easysales.exceptions.KundeExistiertNichtException;

@Service
public class KundeService {

	private final KundenRepo kundenRepo;
	
	
	private final KundeValidation kundeValidation;
	
	
	@Autowired
	public KundeService(KundenRepo kundenRepo, KundeValidation kundeValidation) {
		this.kundenRepo = kundenRepo;
		this.kundeValidation = kundeValidation;
	}
	
	@Transactional(readOnly=true)
	public Kunde getKunde(Long id) throws KundeExistiertNichtException {
		Optional<Kunde> optk = kundenRepo.findById(id);
		
		if(optk.isEmpty()) {
			throw new KundeExistiertNichtException("Ein Kunde mit der ID " + id + " konnte nicht in der Datenbank gefunden werden!");
		}else {
			return optk.get();
		}
	}

	@Transactional(readOnly=true)
	public List<Kunde> getAlleKunden(){
		return kundenRepo.findAll();
//		return kundenRepo.findAll(Sort.by(Sort.Direction.ASC, "nachname"));
	}
	
	@Transactional
	public boolean kundeEintragen(KundeEintragenWebDTO kundeWebDTO) {
		boolean retVal;
		Kunde kunde = new Kunde(kundeWebDTO);
		if(retVal = kundeValidation.validate(kunde)) {
			kundenRepo.save(kunde);	
		}
		return retVal;		
	}
	
	@Transactional
	public boolean kundeEintragen(Kunde kunde) {
		boolean retVal;
		if(retVal = kundeValidation.validate(kunde)) {
			kundenRepo.save(kunde);
		}
		return retVal;
	}
	
	
//	@Transactional
//	public boolean kundeAendern(Long id, KundeAendernWebDTO kundeAendernWebDTO) 
//			throws KundeExistiertNichtException {
//
//		Optional<Kunde> optk = kundenRepo.findById(id);
//		if(optk.isEmpty()) {
//			// User gibt id nich selbst ein! 
//			throw new KundeExistiertNichtException("Ein Kunde mit der ID " + id + " konnte nicht in der Datenbank gefunden werden!");
//		}else {
//			Kunde k = optk.get();
//			k.aendern(kundeAendernWebDTO);
//			boolean retVal;
//			
//			if(retVal = kundeValidation.validate(k)) {
//				kundenRepo.save(k);
//			}
//			return retVal;
//		}	
//	}
	
}
