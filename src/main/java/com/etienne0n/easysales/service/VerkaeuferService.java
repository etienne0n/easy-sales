package com.etienne0n.easysales.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etienne0n.easysales.domain.dto.VerkaeuferEintragenWebDTO;
import com.etienne0n.easysales.domain.entities.Verkaeufer;
import com.etienne0n.easysales.domain.repos.VerkaeuferRepo;
import com.etienne0n.easysales.domain.validation.VerkaeuferValidation;
import com.etienne0n.easysales.exceptions.VerkaeuferExistiertNichtException;


@Service
public class VerkaeuferService {
	
	private final VerkaeuferRepo verkaeuferRepo;
	private final VerkaeuferValidation verkaeuferValidation;
	

	@Autowired
	public VerkaeuferService(VerkaeuferRepo verkaeuferRepo, 
			VerkaeuferValidation verkaeuferValidation) {
		this.verkaeuferRepo = verkaeuferRepo;
		this.verkaeuferValidation = verkaeuferValidation;
	}
	
	@Transactional(readOnly=true)
	public List<Verkaeufer> getAlleVerkaeufer(){
		return verkaeuferRepo.findAll();
//		return verkaeuferRepo.findAll(Sort.by(Sort.Direction.ASC, "nachname"));
	}
	
	@Transactional(readOnly=true)
	public Verkaeufer getVerkaeufer(Long id) throws VerkaeuferExistiertNichtException {
		Optional<Verkaeufer> optv = verkaeuferRepo.findById(id);
		
		if(optv.isEmpty()) {
			throw new VerkaeuferExistiertNichtException("Ein Verkäufer mit der ID " + id + " konnte nicht in der Datenbank gefunden werden!");
		}else {
			return optv.get();
		}
	}
	
	@Transactional
	public boolean verkaeuferEintragen(VerkaeuferEintragenWebDTO verkaeuferWebDTO) {
		boolean retVal;
		Verkaeufer verkaeufer = new Verkaeufer(verkaeuferWebDTO);
		if(retVal = verkaeuferValidation.validate(verkaeufer)) {
			verkaeuferRepo.save(verkaeufer);
		}
		return retVal;
	}
	
	@Transactional
	public boolean verkaeuferEintragen(Verkaeufer verkaeufer) {
		boolean retVal;
		if(retVal = verkaeuferValidation.validate(verkaeufer)) {
			verkaeuferRepo.save(verkaeufer);
		}
		return retVal;
	}
	
	

	

	
	
	
	
	
	
	
}
