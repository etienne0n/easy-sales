package com.etienne0n.easysales.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etienne0n.easysales.domain.dto.ProduktEintragenWebDTO;
import com.etienne0n.easysales.domain.entities.Produkt;
import com.etienne0n.easysales.domain.repos.ProduktRepo;
import com.etienne0n.easysales.domain.validation.ProduktValidation;
import com.etienne0n.easysales.exceptions.ProduktExistiertNichtException;

@Service
public class ProduktService {
	private final ProduktRepo produktRepo;
	private final ProduktValidation produktValidation;
	
	@Autowired
	public ProduktService(ProduktRepo produktRepo, ProduktValidation produktValidation) {
		this.produktRepo = produktRepo;
		this.produktValidation = produktValidation;
	}
	
	
	@Transactional(readOnly=true)
	public List<Produkt> getAlleProdukte(){
		return produktRepo.findAll();
//		return produktRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
	}
	
	@Transactional(readOnly=true)
	public Produkt getProdukt(Long id) throws ProduktExistiertNichtException {
		Optional<Produkt> p = produktRepo.findById(id);
		if(p.isEmpty()) {
			throw new ProduktExistiertNichtException("Ein Produkt mit der id " + id + " konnte nicht in der Datenbank gefunden werden!");
		}else {
			return p.get();
		}
		
	}
	
	
	@Transactional
	public boolean produktEintragen(ProduktEintragenWebDTO produktWebDTO) {
		boolean retVal;
		Produkt p = new Produkt(produktWebDTO);
		if(retVal = produktValidation.validate(p)) {
			produktRepo.save(p);
		}
		
		return retVal;
	}
	
	@Transactional
	public boolean produktEintragen(Produkt produkt) {
		boolean retVal;
		if(retVal = produktValidation.validate(produkt)) {
			produktRepo.save(produkt);
		}
		return retVal;
	}
	
}
