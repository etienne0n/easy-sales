package com.etienne0n.easysales.util;

import java.util.Date;

public final class Dates {
	private Dates() {}
	
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

	private static final Long DAY_IN_S = 86400l;
	private static final Long DAY_IN_MS = 1000 * DAY_IN_S;

	public synchronized static Date getDateXDaysAgo(int days) {
		// convert days to a positive number if negative
		return new Date(System.currentTimeMillis() - 
				(days < 0 ? -days : days) * DAY_IN_MS);
		
	}
	
	
}
