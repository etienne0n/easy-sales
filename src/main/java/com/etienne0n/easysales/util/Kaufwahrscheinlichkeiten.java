package com.etienne0n.easysales.util;

public final class Kaufwahrscheinlichkeiten {
	private Kaufwahrscheinlichkeiten() {}
	public static final int P_0 = 0;
	public static final int P_20 = 20;
	public static final int P_40 = 40;
	public static final int P_60 = 60;
	public static final int P_80 = 80;
	public static final int P_100 = 100;
}
