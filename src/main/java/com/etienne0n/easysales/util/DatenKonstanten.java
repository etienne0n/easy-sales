package com.etienne0n.easysales.util;

public final class DatenKonstanten {
	private DatenKonstanten() {}
	
	/**
	 * Maximale Zeichenlaenge fuer die Beschreibung eines Themas
	 */
	public static final int BESCHREIBUNG_MAX_LEN = 900;

	/**
	 * Mindestlaenge fuer eine Telefonnummer
	 */
	public static final int TEL_MIN_LEN = 6;
	
	/**
	 * Regex fuer eine Telefonnummer
	 */
	public static final String TEL_REGEX = "^[0-9]{" + TEL_MIN_LEN + ",}$";
}
