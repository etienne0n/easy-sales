package com.etienne0n.easysales;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
@SpringBootApplication
public class EasySalesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasySalesApplication.class, args);
	}

	
	@Bean
	@Profile({"test1", "test2"})
	public CommandLineRunner argsPrinter() {
		return (args) -> {
			System.out.print("parsed arguments: ");
			Arrays.stream(args).forEach(a -> System.out.print(a + " "));
			System.out.println();
		};
	}
	
	
	
	
	
	/*
	 * Hinweis 1:
	 * ========
	 * Hibernate Sequence Problem (Id Konflikte) 
	 * bei Neueintrag bei schon vorhandenen Daten mit 
	 * ALTER SEQUENCE HIBERNATE_SEQUENCE RESTART WITH 10001 
	 * geloest.
	 */
	

	
	/*
	 * Hinweis 2:
	 * ========
	 * JSP
	 * 
	 * "It's an older code, sir, but it checks out. I was about to clear them." ;)
	 */



	
}
