package com.etienne0n.easysales.exceptions;

public class ProduktExistiertNichtException extends Exception{

	private static final long serialVersionUID = -1053354531044516993L;

	public ProduktExistiertNichtException(String msg) {
		super(msg);
	}
	
	public ProduktExistiertNichtException() {
		super();
	}
	
}
