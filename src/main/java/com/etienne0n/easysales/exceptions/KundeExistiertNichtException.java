package com.etienne0n.easysales.exceptions;

public class KundeExistiertNichtException extends Exception {

	private static final long serialVersionUID = -8486308218408638974L;
	
	
	public KundeExistiertNichtException(String msg) {
		super(msg);
	}
	
	public KundeExistiertNichtException() {
		super();
	}

}
