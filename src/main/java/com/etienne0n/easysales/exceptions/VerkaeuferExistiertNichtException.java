package com.etienne0n.easysales.exceptions;

public class VerkaeuferExistiertNichtException extends Exception {

	private static final long serialVersionUID = -448359072246660117L;
	
	public VerkaeuferExistiertNichtException(String msg) {
		super(msg);
	}
	
	public VerkaeuferExistiertNichtException() {
		super();
	}

}
