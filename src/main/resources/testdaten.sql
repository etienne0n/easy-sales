/* H2 TESTDATEN - Werden im Profil "test2" mit eingebunden */
/* KUNDEN */

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(1, 'Adalbert', 'Ahnung', '2020-10-8', 'Antwerpener Fahrradwerke', 58653, 'achimstraße 99');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(2, 'Betram', 'Bohnenstange', '1999-9-9', 'Bilsener Wurstfabrikate', 37127, 'Bertechsgardener Zockweg 1');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(3, 'Claas', 'Clever', '2011-11-11', 'Claas Clever AG', 81222, 'Entenweg 77');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(4, 'Dorothe', 'Dröge', '2010-10-10', 'Diestel GmbH', 48155, 'Dinkelbrotstraße 52');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(5, 'Eggbert', 'Ehnich', '2017-7-7', 'Eierwurfmaschinen AG', 20222, 'Eckstraße 70');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(6, 'Fridolin', 'Feierlaune', '2019-4-2', 'Fahrradwerke Antwerpen', 66543, 'Fröhlichweg 96000');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(7, 'Gustav', 'Gans', '2020-7-2', 'Gute Dinge EK', 12334, 'Goldstraße 1000');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(8, 'Hugo', 'Hegel', '2020-2-15', 'Hoch-und-Tiefbau AG', 29123, 'Herkulesallee 2000');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(9, 'Ignatz', 'von Ignatz', '2009-1-1', 'Interkontinentaler Strohhalm Versand', 44284, 'Innosweg 32');

insert into kunde(id, vorname, nachname, kunde_seit, firma, plz, strasse_hausnummer) 
values(10, 'Joseph', 'Jung', '1985-10-8', 'Jülicher Joghurt Walzwerke', 35874, 'Jowasgeht 3');

/* KUNDE_TELEFON */

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(200, '102030', 1);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(201, '203040', 2);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(202, '304050', 3);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(203, '405060', 4);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(204, '506070', 5);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(205, '607080', 6);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(206, '708090', 7);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(207, '8090100', 8);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(208, '90100110', 9);

insert into kunde_telefon(id, telefonnummer, kunde_id)
values(209, '100110120', 10);

/* KUNDE_EMAIL */

insert into kunde_email(id, email, kunde_id)
values(500, 'adalbert.ahnung@sales.de', 1);

insert into kunde_email(id, email, kunde_id)
values(501, 'bertram.bohnenstange@sales.de', 2);

insert into kunde_email(id, email, kunde_id)
values(502, 'claas.clever@sales.de', 3);

insert into kunde_email(id, email, kunde_id)
values(503, 'dorothe.droege@sales.de', 4);

insert into kunde_email(id, email, kunde_id)
values(504, 'eggbert.ehnich@sales.de', 5);

insert into kunde_email(id, email, kunde_id)
values(505, 'fridolin.feierlaune@sales.de', 6);

insert into kunde_email(id, email, kunde_id)
values(506, 'gustav.gansg@sales.de', 7);

insert into kunde_email(id, email, kunde_id)
values(507, 'hugo.hegel@sales.de', 8);

insert into kunde_email(id, email, kunde_id)
values(508, 'ignatz.vonignatz@sales.de', 9);

insert into kunde_email(id, email, kunde_id)
values(509, 'j.jung@sales.de', 10);

insert into kunde_email(id, email, kunde_id)
values(510, 'joseph.jung@web.org', 10);

/* VERKÄUFER */

insert into verkaeufer(id, vorname, nachname, plz, strasse_hausnummer, beschaeftigt_seit)
values(1000, 'Agur', 'Ottenbach', 32453, 'Kohlmeisenweg 55', '2020-1-1');

insert into verkaeufer(id, vorname, nachname, plz, strasse_hausnummer, beschaeftigt_seit)
values(1001, 'Bruma', 'Paulsen', 55477, 'Wachtelweg 9', '2015-10-11');

insert into verkaeufer(id, vorname, nachname, plz, strasse_hausnummer, beschaeftigt_seit)
values(1002, 'Charlie', 'Qutip', 22554, 'Sperberstraße 77', '2019-4-20');

/* VERKÄUFER EMAIL */

insert into verkaeufer_email(id, email, verkaeufer_id)
values(640, 'agur.ottenbach@rudi.de', 1000);

insert into verkaeufer_email(id, email, verkaeufer_id)
values(641, 'a.ottenbach@web.de', 1000);

insert into verkaeufer_email(id, email, verkaeufer_id)
values(642, 'bruma.paulsen@web.de', 1001);

insert into verkaeufer_email(id, email, verkaeufer_id)
values(643, 'charlie.qutip@web.de', 1002);

/* VERKÄUFER TELEFON */

insert into verkaeufer_telefon(id, telefonnummer, verkaeufer_id)
values(240, '01721234567', 1000);

insert into verkaeufer_telefon(id, telefonnummer, verkaeufer_id)
values(241, '0301293612', 1001);

insert into verkaeufer_telefon(id, telefonnummer, verkaeufer_id)
values(242, '0162129317', 1002);



/* GESPRÄCHE */

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4000, '2020-09-02 16:00', 1, 1000);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4001, '2020-10-03 10:00', 4, 1001);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4002, '2020-09-20 12:00', 10, 1002);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4003, '2020-09-15 14:00', 3, 1002);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4004, '2020-08-02 08:30', 2, 1000);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4005, '2020-08-03 09:00', 5, 1001);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4006, '2020-07-20 17:00', 9, 1002);

insert into gespraech(id, datum, kunde_id, verkaeufer_id)
values(4007, '2020-06-15 12:00', 4, 1002);


/* THEMEN */
insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7070, 'Adalbert Ahnung 60 Bleistifte verkaufen', 3000, 80, 4000);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7071, 'Dorothe Dröge benötigt dringend eine Brotschneidegabel', 500, 20, 4001);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7072, 'Joseph Jung Bierdosenhalter für seine Gehhilfe verkaufen', 300, 80, 4002);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7073, 'Claas Clever braucht eine Zeitmaschine um mehr Geld als Dagobert Duck zu verdienen!', 2400000000, 80, 4003);
/* */
insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7074, 'Bertram Bohnenstange will T-800', 100000000, 100, 4004);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7075, 'Eggbert Ehnich will eine Entwicklertasse', 1000, 0, 4005);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7076, 'Ignatz von Ignatz möchte Fahrradlampe kaufen', 800, 100, 4006);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7077, 'Dorothe Dröge braucht einen neuen Malkasten', 1200, 0, 4007);

insert into thema(id, beschreibung, auftragswert, wahrscheinlichkeit, gespraech_id)
values(7078, 'Dorothe Dröge benötigt auch Bleistift', 35, 80, 4001);

/* PRODUKTE */

insert into produkt(id, name) values (6000, 'Fahrradlampe');
insert into produkt(id, name) values (6001, 'Benzinkanister');
insert into produkt(id, name) values (6002, 'Bierdosenhalter für Gehhilfe');
insert into produkt(id, name) values (6003, 'Brotschneidegabel');
insert into produkt(id, name) values (6004, 'Malkasten');
insert into produkt(id, name) values (6005, 'Cyberdine T-800 Infiltratorcyborg');
insert into produkt(id, name) values (6006, 'Fernbedienung für alles');
insert into produkt(id, name) values (6007, 'Zeitmaschine');
insert into produkt(id, name) values (6008, 'Duplex Verlegekabel Cat7 300m');
insert into produkt(id, name) values (6009, 'Bleistift');
insert into produkt(id, name) values (6010, 'Entwicklertasse ''cannot c#''');

/* THEMA_PRODUKTE */

insert into thema_produkte(thema_id, produkte_id)
values(7070, 6009);

insert into thema_produkte(thema_id, produkte_id)
values(7071, 6003);

insert into thema_produkte(thema_id, produkte_id)
values(7072, 6002);

insert into thema_produkte(thema_id, produkte_id)
values(7073, 6007);

insert into thema_produkte(thema_id, produkte_id)
values(7074, 6005);

insert into thema_produkte(thema_id, produkte_id)
values(7075, 6010);

insert into thema_produkte(thema_id, produkte_id)
values(7076, 6000);

insert into thema_produkte(thema_id, produkte_id)
values(7077, 6004);

insert into thema_produkte(thema_id, produkte_id)
values(7078, 6009);

/* ALTER SEQUENCE UM ID KONFLIKTE BEI NEUEN RECORDS ZU VERHINDERN */
alter sequence HIBERNATE_SEQUENCE restart with 10001;
