<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>
	<h2>SELL IT EASY</h2>
<%-- 	<h5><i>${message}</i></h5> --%>

	<form id="kunde_eintragen" action="kunden/kunde_eintragen">
		<input type="submit" value="Neuen Kunden anlegen">
	</form>

	<form id="verkaeufer_eintragen" action="verkaeufer/verkaeufer_eintragen">
		<input type="submit" value="Neuen Verkäufer anlegen">
	</form>
	
	<form id="produkt_eintragen" action="produkte/produkt_eintragen">
		<input type="submit" value="Neues Produkt anlegen">
	</form>
<!-- 	--------------------------------------- -->
<!-- 	<form id="kunden_verwalten" action="kunden/kunden_verwalten"> -->
<!-- 		<input type="submit" value="Kunden verwalten"> -->
<!-- 	</form> -->
	
<!-- 	<form id="verkaeufer_verwalten" action="verkaeufer/verkaeufer_verwalten"> -->
<!-- 		<input type="submit" value="Verkäufer verwalten"> -->
<!-- 	</form> -->
	
<!-- 	<form id="produkte_verwalten" action="produkte/produkte_verwalten"> -->
<!-- 		<input type="submit" value="Produkte verwalten"> -->
<!-- 	</form> -->
	---------------------------------------
	<form id="alle_kunden_anzeigen" action="kunden/alle_kunden_anzeigen">
		<input type="submit" value="Alle Kunden anzeigen">
	</form>
	
	<form id="alle_verkaeufer_anzeigen" action="verkaeufer/alle_verkaeufer_anzeigen">
		<input type="submit" value="Alle Verkäufer anzeigen">
	</form>
	
	<form id="alle_produkte_anzeigen" action="produkte/alle_produkte_anzeigen">
		<input type="submit" value="Alle Produkte anzeigen">
	</form>
</body>
</html>