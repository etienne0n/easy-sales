<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>
	
	<h2>SELL IT EASY</h2>
	<h3>Ein neues Produkt anlegen</h3>
	<p><i>${message}</i></p>
	<form id="produkteingabe" method="post" action="produkt_eintragen_proc">

	
		<div style="float:left;">
		  <label for="name">Produktname:</label>
		  <input type="text" id="name" name="name" required>
		</div>
		
		<br style="clear:both;" />

		<input type="submit" value="Produkt anlegen" > 
	</form> 
	<form id="zurueck" method="get" action="/index">
<!-- 		<label for="">Zurück zur Startseite</label><br> -->
		<input type="submit" value="Zurück zur Startseite">
	</form>

</body>
</html>