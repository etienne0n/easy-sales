<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>
	
	<h2>SELL IT EASY</h2>
	<h3>Produkt bearbeiten (Produkt-ID = ${produkt.id})</h3>
<%-- 	<p><i>${message}</i></p> --%>
	<form id="produktbearbeitung" method="post" action="produkt_bearbeiten_proc">
		
		<input type="hidden" id="id" name="id" value="${produkt.id}">
				
		<div style="float:left;margin-right:20px;">
		  <label for="name">Produkt Name:</label>
		  <input type="text" id="name" name="name" value="${produkt.name}">
		</div>
		
		<br style="clear:both;" />
		<input type="submit" value="Produkt ändern" >
	</form> 
	<form id="zurueck" method="get" action="/index">
		<input type="submit" value="Zurück zur Startseite">
	</form>

</body>
</html>