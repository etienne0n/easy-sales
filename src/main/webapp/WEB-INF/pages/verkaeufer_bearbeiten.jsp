<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>
	
	<h2>SELL IT EASY</h2>
	<h3>Verkaeufer bearbeiten (Verkäufer-ID = ${verkaeufer.id})</h3>
<%-- 	<p><i>${message}</i></p> --%>
	<form id="verkaeuferbearbeitung" method="post" action="verkaeufer_bearbeiten_proc">
		
		<input type="hidden" id="id" name="id" value="${verkaeufer.id}">
				
		<div style="float:left;margin-right:20px;">
		  <label for="vorname">Vorname:</label>
		  <input type="text" id="vorname" name="vorname" value="${verkaeufer.vorname}">
		</div>
		
		<div style="float:left;">
		  <label for="nachname">Nachname:</label>
		  <input type="text" id="nachname" name="nachname" value="${verkaeufer.nachname}">
		</div>

		<br style="clear:both;" />

		<div style="float:left;margin-right:20px;">
		  <label for="nachname">PLZ:</label>
		  <input type="text" id="plz" name="plz" value="${verkaeufer.plz}">
		</div>
		
		<div style="float:left;">
		  <label for="firma">Straße und Hausnummer:</label>
		  <input type="text" id="strasseHausnummer" name="strasseHausnummer" value="${verkaeufer.strasseHausnummer}">
		</div>

		<br style="clear:both;" />
		
		<input type="submit" value="Verkäufer ändern" >
	</form> 
	<form id="zurueck" method="get" action="/index">
		<input type="submit" value="Zurück zur Startseite">
	</form>

</body>
</html>