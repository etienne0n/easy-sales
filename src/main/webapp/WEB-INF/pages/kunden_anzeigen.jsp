<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="
	java.util.Set, 
	com.etienne0n.easysales.domain.entities.KundeEmail
	" 
%>
<!DOCTYPE html>
<!-- JSTL LIBRARIES -->
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>

<c:forEach items="${alle_kunden}" var="kunde" varStatus="outer_loop">
	<form id="kunde_bearbeiten" method="post" action="/kunden/kunde_bearbeiten">
		<b>(ID : ${kunde.id}) | Kunde: ${kunde.vorname} ${kunde.nachname} | Firma : ${kunde.firma} | </b>
		Adresse : ${kunde.plz}, ${kunde.strasseHausnummer} <br>
		Kunde seit: <fmt:formatDate value="${kunde.kundeSeit}" var="formattedDate" type="date" pattern="dd.MM.yyyy" /> ${formattedDate} <br>
		Telefonnummern : 
		<c:forEach items="${kunde.telefonNummern}" var="t" varStatus="loop">
			<input type="hidden" id="telefon_ids" name="telefon_ids" value="${t.id}">
			${t.telefonnummer}
			<c:if test="${!loop.last}">,</c:if>
		</c:forEach>
		<br>
		Email Adressen : 
		<c:forEach items="${kunde.emailAdressen}" var="e" varStatus="loop">
			<input type="hidden" id="email_ids" name="email_ids" value="${e.id}">
			${e.email}
			<c:if test="${!loop.last}">,</c:if>
		</c:forEach>
		<br>

	
		<input type="hidden" id="id" name="id" value="${kunde.id}">
		<input type="hidden" id="vorname" name="vorname" value="${kunde.vorname}">
		<input type="hidden" id="nachname" name="nachname" value="${kunde.nachname}">
		<input type="hidden" id="firma" name="firma" value="${kunde.firma}">
		<input type="hidden" id="plz" name="plz" value="${kunde.plz}">
		<input type="hidden" id="strasseHausnummer" name="strasseHausnummer" value="${kunde.strasseHausnummer}">
		
<%-- 		<c:forEach items="${kunde.emailAdressen}" var="e" varStatus="loop"> --%>
<%-- 			<input type="hidden" id="email_ids" name="email_ids" value="${e.id}"> --%>
<%-- 		</c:forEach> --%>
<%-- 		<c:forEach items="${kunde.telefonnummern}" var="t" varStatus="loop"> --%>
<%-- 			<input type="hidden" id="telefon_ids" name="telefon_ids" value="${t.id}"> --%>
<%-- 		</c:forEach> --%>
		
		<input type="submit" value="bearbeiten">
	</form>
	<c:if test="${!outer_loop.last}">
	================================================<br>
	</c:if>
	
</c:forEach>

</body>
</html>