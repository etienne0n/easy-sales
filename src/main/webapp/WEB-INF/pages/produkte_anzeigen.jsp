<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- JSTL LIBRARIES -->
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>

<c:forEach items="${alle_produkte}" var="produkt" varStatus="outer_loop">
	<b>(ID : ${produkt.id}) | Produkt: ${produkt.name} </b><br>
	<form id="produkt_bearbeiten" method="post" action="/produkte/produkt_bearbeiten">
		<input type="hidden" id="id" name="id" value="${produkt.id}">
		<input type="hidden" id="name" name="name" value="${produkt.name}">
		<input type="submit" value="bearbeiten">
	</form>
	<c:if test="${!outer_loop.last}">
	================================================<br>
	</c:if>
	
</c:forEach>

</body>
</html>