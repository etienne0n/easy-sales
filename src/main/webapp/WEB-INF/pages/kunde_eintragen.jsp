<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>
	
	<h2>SELL IT EASY</h2>
	<h3>Einen neuen Kunden anlegen</h3>
	<p><i>${message}</i></p>
	<form id="kundeneingabe" method="post" action="kunde_eintragen_proc">

	
		<div style="float:left;margin-right:20px;">
		  <label for="vorname">Vorname:</label>
		  <input type="text" id="vorname" name="vorname" required>
		</div>
		
		<div style="float:left;margin-right:20px;">
		  <label for="nachname">Nachname:</label>
		  <input type="text" id="nachname" name="nachname" required>
		</div>
		
		<div style="float:left;">
		  <label for="firma">Firma:</label>
		  <input type="text" id="firma" name="firma" required>
		</div>

		<br style="clear:both;" />

		
		<div style="float:left;margin-right:20px;">
		  <label for="nachname">PLZ:</label>
		  <input type="text" id="plz" name="plz" required>
		</div>
		
		<div style="float:left;">
		  <label for="firma">Straße und Hausnummer:</label>
		  <input type="text" id="strasseHausnummer" name="strasseHausnummer" required>
		</div>

		<br style="clear:both;" />

		
		<div style="float:left;">
			<label for="kundeSeit">Kunde seit:</label>
			<input required type="date" id="kundeSeit" name="kundeSeit" value='${now}' max='${now}'>
		</div>
		<br style="clear:both;" />

		
		<div style="float:left;">
			<label for="telefonnummer">Telefonnummern: (Nur Ziffern, keine Leerzeichen, Mindestlänge = ${tel_min_len})</label>
			<input type="tel" id="telefonnummer1" name="telefonnummer1" pattern='${tel_pattern}' placeholder="Bitte eintragen" required>
			<input type="tel" id="telefonnummer2" name="telefonnummer2" pattern='${tel_pattern}' placeholder="optional">
			<input type="tel" id="telefonnummer3" name="telefonnummer3" pattern='${tel_pattern}' placeholder="optional">
		</div>
		<br style="clear:both;" />
	
		
		<div style="float:left;">
			<label for="email">Email Adressen:</label>
			<input type="email" id="email1" name="email1" placeholder="Bitte eintragen" required>
			<input type="email" id="email2" name="email2" placeholder="optional">
			<input type="email" id="email3" name="email3" placeholder="optional">
		</div>
		<br style="clear:both;" />
		
		<input type="submit" value="Kunden anlegen" >
	</form> 
	<form id="zurueck" method="get" action="/index">
		<input type="submit" value="Zurück zur Startseite">
	</form>

</body>
</html>