<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- JSTL LIBRARIES -->
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>

<c:forEach items="${alle_verkaeufer}" var="verkaeufer" varStatus="outer_loop">
	<b>(ID : ${verkaeufer.id}) | Verkäufer: ${verkaeufer.vorname} ${verkaeufer.nachname} </b><br>
	Adresse : ${verkaeufer.plz}, ${verkaeufer.strasseHausnummer} <br>
	Beschäftigt seit: <fmt:formatDate value="${verkaeufer.beschaeftigtSeit}" var="formattedDate" type="date" pattern="dd.MM.yyyy" /> ${formattedDate} <br>
	Telefonnummern : 
	<c:forEach items="${verkaeufer.telefonNummern}" var="t" varStatus="loop">
		${t.telefonnummer}
		<c:if test="${!loop.last}">,</c:if>
	</c:forEach>
	<br>
	Email Adressen : 
	<c:forEach items="${verkaeufer.emailAdressen}" var="e" varStatus="loop">
		${e.email}
		<c:if test="${!loop.last}">,</c:if>
	</c:forEach>
	<br>
	<form id="verkaeufer_bearbeiten" method="post" action="/verkaeufer/verkaeufer_bearbeiten">
		<input type="hidden" id="id" name="id" value="${verkaeufer.id}">
		<input type="hidden" id="vorname" name="vorname" value="${verkaeufer.vorname}">
		<input type="hidden" id="nachname" name="nachname" value="${verkaeufer.nachname}">
		<input type="hidden" id="plz" name="plz" value="${verkaeufer.plz}">
		<input type="hidden" id="strasseHausnummer" name="strasseHausnummer" value="${verkaeufer.strasseHausnummer}">
		<input type="submit" value="bearbeiten">
	</form>
	<c:if test="${!outer_loop.last}">
	================================================<br>
	</c:if>
	
</c:forEach>

</body>
</html>