<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/css/oldschool.css">
<meta charset="UTF-8">
<title>sell it easy</title>
</head>
<body>
	
	<h2>SELL IT EASY</h2>
	<h3>Kunden bearbeiten (Kunden-ID = ${kunde.id})</h3>
<%-- 	<p><i>${message}</i></p> --%>
	<form id="kundenbearbeitung" method="post" action="kunde_bearbeiten_proc">
		
		<input type="hidden" id="id" name="id" value="${kunde.id}">
				
		<div style="float:left;margin-right:20px;">
		  <label for="vorname">Vorname:</label>
		  <input type="text" id="vorname" name="vorname" value="${kunde.vorname}">
		</div>
		
		<div style="float:left;margin-right:20px;">
		  <label for="nachname">Nachname:</label>
		  <input type="text" id="nachname" name="nachname" value="${kunde.nachname}">
		</div>
		
		<div style="float:left;">
		  <label for="firma">Firma:</label>
		  <input type="text" id="firma" name="firma" value="${kunde.firma}">
		</div>

		<br style="clear:both;" />

		
		<div style="float:left;margin-right:20px;">
		  <label for="nachname">PLZ:</label>
		  <input type="text" id="plz" name="plz" value="${kunde.plz}">
		</div>
		
		<div style="float:left;">
		  <label for="firma">Straße und Hausnummer:</label>
		  <input type="text" id="strasseHausnummer" name="strasseHausnummer" value="${kunde.strasseHausnummer}">
		</div>

		<br style="clear:both;" />
		
		<input type="submit" value="Kunden ändern" >
	</form> 
	<form id="zurueck" method="get" action="/index">
		<input type="submit" value="Zurück zur Startseite">
	</form>

</body>
</html>